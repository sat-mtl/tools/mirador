.. container:: index-title

   Mirador

.. container:: index-intro

   welcome to the Mirador documentation website

.. container:: index-section

   what is Mirador?

Mirador is...

.. container:: index-section

   Mirador demo

The following video demonstrates the goal that we have in mind we designing the Mirador hardware and its surrounding software pieces.

.. raw:: html

   <div style="padding:56.25% 0 0 0;position:relative;"><iframe title="Splash demo video" src="https://player.vimeo.com/video/549423960?color=ff9933&api=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

.. container:: index-section

   why Mirador?

Mirador was started because...

.. container:: index-section

   get in touch

This free, libre open source software was made by the Metalab team.

To know more about us, visit `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__.

Get in touch with us by `asking questions on the issue tracker <https://gitlab.com/sat-metalab/hardware/mirador/issues>`__.
  
.. container:: index-section

   contribute to the project

If you wish to contribute to the project, please read the `Code of Conduct <CODEOFCONDUCT>`__ and the `Contributing <CONTRIBUTING>`__ guide.
  
.. container:: index-section

   please show me the data!
   
For more information visit `the code repository <https://gitlab.com/sat-metalab/hardware/mirador>`__.
  
.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technologies <http://www.sat.qc.ca>`__ (also known as SAT).
