Splash
======
   
.. toctree::
   :maxdepth: 1
   :caption: On this site:
   
   install/contents
   tutorials/contents
   reference/contents
   howto/contents
   explanations/contents

   news
   community
   
   faq/contents
   
   contact

Welcome to the Splash documentation website.

.. figure:: _static/images/splash_sato.jpg
   :alt: Splash in a 20 meters fulldome

   Splash in a 20 meters fulldome

Description of the project
--------------------------

Overview
~~~~~~~~

Splash is a free (as in GPL) modular mapping software. Using a
user-provided 3D model with a UV mapping of the projection surface, it
will handle multiple inputs that are mapped on multiple 3D models,
taking care of calibrating the videoprojectors, and feeding them with
the video input sources. It currently runs on a single computer but
support for multiple computers is planned. It has been tested with up to
eight outputs on two graphics cards.

What is modular mapping?
^^^^^^^^^^^^^^^^^^^^^^^^

TODO

Technical specifications
~~~~~~~~~~~~~~~~~~~~~~~~

Although Splash was primarily targeted toward fulldome mapping and has
been extensively tested in this context, it can be used for virtually
any surface provided that a 3D model of the geometry is available.
Multiple fulldomes have been mapped, either by the authors of this
software (two small dome (3m wide) with 4 projectors, a big one (20m
wide) with 8 projectors) or by other teams. It has also been tested
sucessfully as a more regular video-mapping software to project on
buildings, or `onto moving objects <https://vimeo.com/268028595>`__.

Splash can read videos from various sources amoung which video files
(most common format and Hap variations), video input (such as video
cameras and capture cards), and
`Shmdata <https://gitlab.com/sat-metalab/shmdata>`__ (a shared memory
library used to make softwares from the SAT Metalab communicate between
each others). An addon for Blender is included which allows for
exporting draft configurations and update in real-time the meshes.
Splash also handles automatically a few things: - semi automatic
geometric calibration of the video-projectors, - projection warping, -
automatic calibration of the blending between projections, -
experimental automatic colorimetric calibration (with a gPhoto
compatible camera)

Demo of the project
-------------------

TODO

Getting started with Splash
---------------------------

You will find all the information necessary to help you get started with
Splash in the following links:

-  `how to install Splash <install/installation.html>`__
-  `first steps with Splash <tutorials/first_steps.html>`__
-  `all of the tutorials <tutorials/index.html>`__
-  `how-to guides <howto/index.html>`__
-  `Frequently Asked Questions (FAQ) <faq/index.html>`__

Get in touch
------------

Want to share what you are currently working on ? Want to chat with the
team and the users community ? Join us !

-  `chat on IRC channel ##splash on
   Freenode <https://webchat.freenode.net/##splash>`__
-  `Gitlab issue
   tracker <https://gitlab.com/sat-metalab/splash/issues>`__

Contribute to the project
-------------------------

Start by reading the `Code of Conduct <CODEOFCONDUCT.html>`__.

Then, you should read the `Contributing guide <CONTRIBUTING.html>`__.

Show me the code!
-----------------

The project repository is available `on
Gitlab <https://gitlab.com/sat-metalab/splash>`__.

Sponsors
--------

This project is made possible thanks to the `Society for Arts and
Technologies <http://www.sat.qc.ca>`__ (also known as SAT).
