
## 3D Model

The 3D Model of the module was made with [Rhinoceros](https://www.rhino3d.com/). 
The Blender equivalent was made easier thanks to the addon [import_3dm](https://github.com/jesterKing/import_3dm).

## Main animation

The main animation was made using Blender and the addon [Projectors](https://github.com/Ocupe/Projectors) to simulate the projectors lighting. 
Proper integration of the installation video is made using the compositor, the animated part is a raw output from Blender with no further alteration.

The demo scene of the animation was made possible thanks to other creators sharing content over Creative Common licenses. 
Specifically, the following material was used or remixed in the process:
- Assets
	- [Lowpoly People](https://skfb.ly/6zqCE) from Loïc Norgeot
	- [Office Chair](https://skfb.ly/6VLRV) by kreems
	- [Table](https://skfb.ly/6ZppI) by dolhopoloffs
	- [Office Plants](https://skfb.ly/6WXNo) by polza
	- [Office Plant](https://skfb.ly/6VZM8) by VIS-ALL-3D
	- [Vending Machine](https://skfb.ly/6YXx9) by Greg Thorburn
	- [Tripod](https://blendswap.com/blend/15881) by zawhatthe
- Textures are from <https://cc0textures.com/>, <https://texturehaven.com/> and <https://3dtextures.me/>
- Installation video is loop 100 from [Midge "Mantissa" Sinnaeve](https://mantissa.xyz/)

## Corner view

This view was made using a similar scene of the main animation, and the same compositor trick to get a proper projection. 
The EiS example scene for the projection was made by Léa Nugue.
